package com.agiletestingalliance;


import junit.framework.TestCase;


public class MinMaxTest extends TestCase {


    public void testF() {
        MinMax minMax = new MinMax();
        assertEquals(minMax.f(1,2), 2);
        assertEquals(minMax.f(2,1), 2);
    }


    public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals(minMax.bar(""), "");
        assertEquals(minMax.bar("test"), "test");
    }
}
