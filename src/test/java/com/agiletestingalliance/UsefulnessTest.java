package com.agiletestingalliance;

import junit.framework.TestCase;

public class UsefulnessTest extends TestCase {
    private Usefulness usefulness = new Usefulness();

    public void testDesc() {
        String test = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";
        assertEquals(usefulness.desc(), test);
    }
}
